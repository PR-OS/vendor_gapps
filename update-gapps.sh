#!/usr/bin/env bash

TOP=$(pwd)

# check for any connect devices, exit if none
if [[ -z $(adb devices | grep -P '\tdevice') ]]; then
    echo "No adb devices found, is USB debugging enabled and your device is connected?"
    exit 1
fi

echo "Checking for updates.."

BLACKLIST=(
    'photos'
)

checkblacklist() {
    for p in "${BLACKLIST[@]}"; do
        if echo $1 | grep -i $p &>/dev/null; then
            echo "$p in blacklist, skipping"
            return 1
        fi
    done
}

for i in $(find $PWD/app $PWD/priv-app -name *.apk); do

    # package name/path on device
    PACKAGE=$(aapt dump badging $i | head -n 1 | cut -d ' ' -f 2 | sed -n 's/name=//p' | tr -d "'")
    PACKAGE_PATH=$(adb shell dumpsys package $PACKAGE | sed -n 's/^.*path: //p')

    # skip if in blacklist
    checkblacklist $PACKAGE || continue

    # don't include packages from /system, they're already up to date
    # (or if PACKAGE_PATH is blank)
    [[ $PACKAGE_PATH =~ /system || -z $PACKAGE_PATH ]] && continue

    # Old versionCode
    OLD_VERSIONCODE=$(aapt d badging $i | sed -n "s/.*versionCode='//p" | sed -n "s/'.*//p")
    OLD_VERSIONNAME=$(aapt d badging $i | sed -n "s/.*versionName='//p" | sed -n "s/'.*//p")

    DEVICE_VERSIONCODE="$(adb shell dumpsys package $PACKAGE | grep -C 10 codePath=/data | \
        sed -n 's/.*versionCode=//p' | sed -n 's/ .*//p')"
    DEVICE_VERSIONNAME="$(adb shell dumpsys package $PACKAGE | grep -C 10 codePath=/data | \
        sed -n 's/.*versionName=//p')"

    # check if version codes are upgraded
    [[ "$OLD_VERSIONNAME($OLD_VERSIONCODE)" = "$DEVICE_VERSIONNAME($DEVICE_VERSIONCODE)" ]] && continue

    # are we messing with priv-app? we may need to grant permissions!
    [[ $i =~ priv-app ]] && PRIVAPP_CHANGED=true

    # pull gapps from device to /tmp
    cd /tmp
    adb pull $PACKAGE_PATH

    # grab new version numbers before moving
    NEW_VERSIONCODE=$(aapt d badging base.apk | sed -n "s/.*versionCode='//p" | sed -n "s/'.*//p")
    NEW_VERSIONNAME=$(aapt d badging base.apk | sed -n "s/.*versionName='//p" | sed -n "s/'.*//p")

    # version changes for commit message
    COMMIT_MSG+=$(echo -e "\n\n$PACKAGE:\n$OLD_VERSIONNAME($OLD_VERSIONCODE) -> $NEW_VERSIONNAME($NEW_VERSIONCODE)")

    mv base.apk $i
done

cd $TOP
git add --all
git commit --signoff -m "AUTO: Update GApps to latest Play Store releases" -m "$COMMIT_MSG"

if [[ -n $PRIVAPP_CHANGED ]]; then
    echo
    echo "Priv-app apps were updated. Please compile inline and run the"
    echo "priv-app permission script to see if we need to grant permissions!"
fi
